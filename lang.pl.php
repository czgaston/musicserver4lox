<?php
//Polish
//General
$language['GENERAL_SAVE'] = 'Zapisz';
$language['GENERAL_CLOSE'] = 'Zamknij';
$language['GENERAL_YES'] = 'Tak';
$language['GENERAL_NO'] = 'Nie';
$language['GENERAL_MANUAL'] = 'Podręcznik';
$language['GENERAL_FORUM'] = 'Loxforum';
$language['GENERAL_START'] = 'Uruchom';
$language['GENERAL_STOP'] = 'Zatrzymaj';
$language['GENERAL_RESTART'] = 'Uruchom ponownie';
$language['GENERAL_TEST'] = 'Przetestuj';
$language['GENERAL_SETTINGS'] = 'Ustawienia';
$language['GENERAL_EQ'] = 'Equalizer';
$language['GENERAL_TT_START'] = 'Uruchamia usługę';
$language['GENERAL_TT_STOP'] = 'Zatrzymuje usługę';
$language['GENERAL_TT_RESTART'] = 'Zatrzymuje i uruchamia usługę';
$language['GENERAL_TT_TEST'] = 'Testuje urządzenie';
$language['GENERAL_TT_CONTROLELEMENTS'] = 'Kliknij tu po elementy sterujące';
$language['GENERAL_TT_SERVICE_RUNNING'] = 'Usługa jest uruchomiona';
$language['GENERAL_TT_SERVICE_STOPED'] = 'Usługa jest zatrzymana';
$language['GENERAL_TT_SERVICE_PLAYER_STANDBY'] = 'Usługa jest uruchomiona - odtwarzacz jest w trybie gotowości';
$language['GENERAL_TT_SERVICE_REACHABLE'] = 'Usługa jest osiągalna';
$language['GENERAL_TT_SERVICE_UNREACHABLE'] = 'Usługa jest nieosiągalna';
$language['GENERAL_TT_REACHABLE'] = 'osiągalny';
$language['GENERAL_TT_UNREACHABLE'] = 'nie osiągalny';
$language['GENERAL_TT_SETTINGS'] = 'Ustawienia';

//Languages
$language['GENERAL_LANG_DE'] = 'deutsch';
$language['GENERAL_LANG_EN'] = 'english';
$language['GENERAL_LANG_PL'] = 'polski';

// Power
$language['POWER_PAGE_TITLE'] = 'Zasilanie MS4L';
$language['POWER_SHUTDOWN'] = 'Zamknięcie systemu';
$language['POWER_REBOOT'] = 'Restart systemu';
$language['POWER_MODAL_SHUTDOWN_TITLE'] = 'Zamknij system';
$language['POWER_MODAL_SHUTDOWN_MSG'] = 'Czy na pewno zamknąć system?';
$language['POWER_MODAL_REBOOT_TITLE'] = 'Zrestartuj system';
$language['POWER_MODAL_REBOOT_MSG'] = 'Czy na pewno zrestartować system?';
$language['POWER_SHUTDOWN_MSG'] = 'System jest zamykany...';
$language['POWER_REBOOT_MSG'] = 'System jest restartowany...';
$language['POWER_SHUTDOWN_BACK'] = 'Powrót do logowania';

//Navigaion Main
$language['NAV_MAIN_DASHBOARD'] = 'Dashboard';
$language['NAV_MAIN_SETTINGS'] = 'Ustawienia';
$language['NAV_MAIN_SETTINGS_PLAYER'] = 'Strefy';
$language['NAV_MAIN_SETTINGS_PLAYER_INTERNAL'] = 'wewnętrzne';
$language['NAV_MAIN_SETTINGS_PLAYER_EXTERNAL'] = 'zewnętrzne';
$language['NAV_MAIN_SETTINGS_EQUALIZER'] = 'Equalizer';
$language['NAV_MAIN_SETTINGS_TTS'] = 'TTS/Alarm/Dzwonek';
$language['NAV_MAIN_SETTINGS_SERVICES'] = 'Usługi';
$language['NAV_MAIN_SETTINGS_SUB_TTS'] = 'TTS/CTS';
$language['NAV_MAIN_SETTINGS_SUB_T2T'] = 'Travel2TTS (t2t)';
$language['NAV_MAIN_SETTINGS_SUB_W2T'] = 'Weather2TTS (w2t)';
$language['NAV_MAIN_SETTINGS_SUB_RING'] = 'Dzwonek/Alarm/Budzik';
$language['NAV_MAIN_SETTINGS_SERVICES_SUB_PM'] = 'PowerManager';
$language['NAV_MAIN_SETTINGS_SERVICES_SUB_SQ2LOX'] = 'Squeeze2Lox / MusicServerGateway';
$language['NAV_MAIN_SETTINGS_SERVICES_SUB_FM'] = 'FollowMe';
$language['NAV_MAIN_SETTINGS_SERVICES_SUB_FC'] = 'FritzCallMonitor';
$language['NAV_MAIN_SETTINGS_SERVICES_SUB_T5'] = 'Odbiornik T5';
$language['NAV_MAIN_SETUP'] = 'Setup';
$language['NAV_MAIN_SETUP_SOUNDCARD'] = 'Karty dźwiękowe';
$language['NAV_MAIN_SETUP_NETWORK'] = 'Sieć';
$language['NAV_MAIN_SETUP_NETWORK_SETTINGS'] = 'Ustawienia sieciowe';
$language['NAV_MAIN_SETUP_NETWORK_MOUNT'] = 'Podłączanie udziału sieciowego';
$language['NAV_MAIN_SETUP_EMAIL'] = 'eMail';
$language['NAV_MAIN_SETUP_STATISTICS'] = 'Statystyki';
$language['NAV_MAIN_SETUP_LMS'] = 'Logitech Media Server';
$language['NAV_MAIN_SETUP_MINISERVER'] = 'MiniServer';
$language["NAV_MAIN_SETUP_PLAYER"] = 'Strefy';
$language["NAV_MAIN_SETUP_PLAYER_INTERNAL"] = 'wewnętrzne';
$language["NAV_MAIN_SETUP_PLAYER_EXTERNAL"] = 'zewnętrzne';
$language['NAV_MAIN_TOOLS'] = 'Narzędzia';
$language['NAV_MAIN_TOOLS_UPDATE'] = 'Aktualizacja';
$language['NAV_MAIN_TOOLS_BACKUP'] = 'Kopia zapasowa';
$language['NAV_MAIN_TOOLS_SERVERRESET'] = 'resetowanie MS4L';
$language['NAV_MAIN_TOOLS_LOXHELPER'] = 'Lox-Config Helper';
$language['NAV_MAIN_TOOLS_LOXHELPER_EVENTCREATOR'] = 'Event Creator';
$language['NAV_MAIN_TOOLS_LOXHELPER_TEMPLATECREATOR'] = 'Template Creator';
$language['NAV_MAIN_HELP'] = 'Pomoc';
$language['NAV_MAIN_HELP_MANUAL'] = 'Podręcznik';
$language['NAV_MAIN_HELP_FORUM'] = 'LoxForum';
$language['NAV_MAIN_HELP_SUPPORT_DATA'] = 'Support-Data';
$language["NAV_MAIN_HELP_SUPPORT_LOGVIEWER"] = 'Log-File Viewer';
$language['NAV_MAIN_HELP_ABOUT'] = 'O oprogramowaniu';

//Naviagtion Header
$language['NAV_HEADER_TT_SET_LANGUAGE'] = 'Wybierz wersję językową';
$language['NAV_HEADER_TT_SET_USER'] = 'Ustawienia użytkownika';
$language['NAV_HEADER_TT_UPDATE'] = 'Aktualizacja';
$language['NAV_HEADER_TT_UPDATE_AVAILABLE'] = 'Dostępna aktualizacja';
$language['NAV_HEADER_TT_POWER'] = 'Ustawienia zasilania';
$language['NAV_HEADER_TT_LOGOUT'] = 'Wyloguj użytkownika';

//Login-Page
$language['LOGIN_PAGE_TITLE'] = 'MusicServer4Lox Login';
$language['LOGIN_FORM_TITLE'] = 'Zaloguj się do MusicServera';
$language['LOGIN_FORM_USER'] = 'Nazwa użytkownika';
$language['LOGIN_FORM_PASS'] = 'Hasło';
$language['LOGIN_FORM_FORGOT_PASS'] = 'Zapomniałeś hasła?';
$language['LOGIN_FORM_FORGOT_RESET'] = 'Zresetuj';
$language['LOGIN_FORM_FORGOT_LOGIN'] = 'Zaloguj';
$language['LOGIN_FORM_MODAL_TITLE'] = 'Nazwa użytkownika / hasło niepoprawne';
$language['LOGIN_FORM_MODAL_MSG'] = 'Twoja nazwa użytkownika lub hasło wydają się być niepoprawne!<br><b>Spróbuj ponownie...</b>';

//Password Reset
$language['PASSWDRESE_PAGE_TITLE'] = 'Resetowanie hasła MusicServer4Lox';
$language["PASSWDRESET_TITLE"] = 'Resetowanie hasła';
$language["PASSWDRESET_TEXT"] = 'Jeśli skonfigurowałeś serwer pocztowy, zostanie do ciebie wysłana wiadomość z nowym hasłem.'; 
$language["PASSWDRESET_RESET"] = 'Zresetuj hasło teraz';
$language["PASSWDRESET_MODAL_TITLE_MAILOK"] = 'Wysyłanie eMail OK.';
$language["PASSWDRESET_MODAL_TITLE_MAILNOK"] = 'Wysyłanie eMail zakończyło się niepowodzeniem';
$language["PASSWDRESET_MODAL_MSG_MAILOK"] = 'Wiadomość eMail została wysłana.';
$language["PASSWDRESET_MODAL_MSG_MAILNOK"] = 'Wiadomość eMail nie mogła zostać wysłana.<br>Serwer poczty skonfigurowany?';
$language["PASSWDRESET_MAIL_SUBJECT"] = 'MS4L - nowe hasło';
$language["PASSWDRESET_MAIL_TEXT"] = 'Cześć {realname},<br><br>Zamówiłeś nowe hasło do swojej instalacji MusicServer4Lox.<br><br>	Twoja nazwa użytkownika: {username} <br>Twoje nowe hasło: {password} <br><br>Możesz się już zalogować z użyciem nowego hasła.<br><br>Twój zespół MusicServer4Lox';

//Dashboard (index.php)
$language['DASHBOARD_PAGE_TITLE'] = 'Dashboard MS4L';
$language['DASHBOARD_TITLE'] = 'Dashboard';
$language['DASHBOARD_TITLE_MUSICSERVER'] = 'MusicServer';
$language['DASHBOARD_TITLE_NETWORK'] = 'Sieć';
$language['DASHBOARD_TITLE_SERVICES'] = 'Usługi';
$language['DASHBOARD_TITLE_ZONES'] = 'Strefy';
$language['DASHBOARD_UPTIME'] = 'Uptime';
$language['DASHBOARD_DAY'] = 'dni';
$language['DASHBOARD_TOTAL'] = 'total';
$language['DASHBOARD_FREE'] = 'free';
$language['DASHBOARD_USED'] = 'used';
$language['DASHBOARD_HARDWARE'] = 'Hardware';
$language['DASHBOARD_CPU'] = 'Procesor';
$language['DASHBOARD_RAM'] = 'Pamięć';
$language['DASHBOARD_HDD'] = 'Dysk';
$language['DASHBOARD_OS'] = 'System operacyjny';
$language['DASHBOARD_HOSTNAME'] = 'Nazwa hosta';
$language['DASHBOARD_IP'] = 'Adres IP';
$language['DASHBOARD_NETMASK'] = 'Maska sieci';
$language['DASHBOARD_GATEWAY'] = 'Brama';
$language['DASHBOARD_DNS'] = 'Serwer DNS';
$language['DASHBOARD_INET'] = 'Internet';
$language['DASHBOARD_MINISERVER'] = 'MiniServer';
$language['DASHBOARD_PACKAGES_LOSS']= 'utrata pakietów';
$language['DASHBOARD_MS'] = 'MusicServer';
$language['DASHBOARD_START_ALL'] = 'Uruchomienie wszystkich usług';
$language['DASHBOARD_STOP_ALL'] = 'Zatrzymanie wszystkich usług';
$language['DASHBOARD_RESTART_ALL'] = 'Ponowne uruchomienie wszystkich usług';
$language['DASHBOARD_LMS'] = 'Logitech Media Server';
$language['DASHBOARD_LMS_WEB'] = 'LMS WebUI';
$language['DASHBOARD_LMS_TELNET'] = 'LMS Telnet';
$language['DASHBOARD_PM'] = 'Powermanager';
$language['DASHBOARD_SQ2LOX'] = 'Squeeze2lox / MS-Gateway';
$language['DASHBOARD_FC'] = 'FritzCall-Monitor';
$language['DASHBOARD_T5'] = 'Odbiornik T5';
$language['DASHBOARD_SERVER_NOT_REACHABLE'] = 'Serwer nieosiągalny';
$language['DASHBOARD_NO_INTERNAL_PLAYER'] = 'Brak stref wewnętrznych';
$language['DASHBOARD_NO_EXTERNAL_PLAYER'] = 'Brak stref zewnętrznych';
$language['DASHBOARD_VISIT_WEBUI'] = 'LMS-WebUI';
$language['DASHBOARD_VISIT_WEBUI_SETTINGS'] = 'Ustawienia LMS-WebUI';
$language['DASHBOARD_TT_PLAY'] = 'Odtwarzacz gra muzykę';
$language['DASHBOARD_TT_PAUSE'] = 'Odtwarzacz jest wstrzymany';
$language['DASHBOARD_TT_STOP'] = 'Odtwarzacz jest zatrzymany';
$language['DASHBOARD_TT_VISIT_WEBUI'] = 'Odwiedź LMS-WebUI';
$language['DASHBOARD_TT_VISIT_WEBUI_SETTINGS'] = 'Odwiedź ustawienia LMS-WebUI';

//About
$language['ABOUT_PAGE_TITLE'] = 'O oprogramowaniu MS4L';
$language["ABOUT_TITLE"] = 'O MusicServer4Lox';
$language["ABOUT_TITLE_COPYRIGHT"] = 'Copyright';
$language["ABOUT_COPYRIGHT_MSG"] = '
MusicServer4Lox został stworzony przeze mnie (Dietera Schmidbergera) i zaprezentowany na loxforum.com.
<br>To oprogramowanie jest oferowane bezpłatnie, ale jest chronione prawem autorskim.
<br>To znaczy, że oprogramowanie może być redystrybuowane bezpłatnie, ale modyfikacja kodu jest dozwolona wyłącznie za wyraźną zgodą właściciela praw autorskich.
<br>To dotyczy tylko tych części oprogramowania, które napisałem samodzielnie.';
$language["ABOUT_TITLE_LICENCE"] = 'Licencja';
$language["ABOUT_TITLE_DISCLAIMER"] = 'Zastrzeżenia';
$language["ABOUT_DISCLAIMER_MSG"] = '
Oprogramowanie zostało stworzone zgodnie z moją najlepszą wiedzą i sumieniem, przeze mnie i na własny użytek.
<br>Opublikowałem je w nadziei, że może być użyteczne również dla innych.
<br>Kategorycznie nie obejmuje ono gwarancji na funkcjonalność i zastosowania. Używasz oprogramowania na własne ryzyko!';
$language["SUPPORT_TITLE_CREDITS"] = 'Podziękowania';
$language["SUPPORT_PICTURES"] = 'Zdjęcia';
$language["SUPPORT_TITLE_CODING"] = 'Kodowanie';
$language["SUPPORT_TITLE_TRANSLATION"] = 'Tłumaczenia';

//Settings Player internen
$language['SET_PLAYER_PAGE_TITLE'] = 'Ustawienia strefy MS4L';
$language['SET_PLAYER_TITLE'] = 'Ustawienia strefy (wewnętrznej)';
$language['SET_PLAYER_TITLE_EXT'] = 'Ustawienia strefy (zewnętrznej)';
$language['SET_PLAYER_TITLE_ZONES'] = 'Strefy';
$language['SET_PLAYER_TITLE_ZONE'] = 'Strefa';
$language['SET_PLAYER_TITLE_SOUNDCARD'] = 'Karta dźwiękowa';
$language['SET_PLAYER_TITLE_SQ2LOX'] = 'Sqeeze2Lox';
$language['SET_PLAYER_TITLE_PM'] = 'PowerManager';
$language['SET_PLAYER_DEL'] = 'Usuń strefę';
$language['SET_PLAYER_PLAYERNEW'] = 'Nowa strefa';
$language['SET_PLAYER_MANUAL'] = 'Wprowadź ręcznie nazwę strefy / adres mac';
$language['SET_PLAYER_ZONENUMBER'] = 'Numer strefy';
$language['SET_PLAYER_ZONENAME'] = 'Nazwa strefy';
$language['SET_PLAYER_ZONEMAC'] = 'adres mac';
$language['SET_PLAYER_ZONEALSA'] = 'Ustawienia alsa';
$language['SET_PLAYER_ZONESOUNDCARD'] = 'Karta dźwiękowa strefy';
$language['SET_PLAYER_CH1L'] = 'Kanał 1 lewy';
$language['SET_PLAYER_CH1R'] = 'Kanał 1 prawy';
$language['SET_PLAYER_CH2L'] = 'Kanał 2 lewy';
$language['SET_PLAYER_CH2R'] = 'Kanał 2 prawy';
$language['SET_PLAYER_CH3L'] = 'Kanał 3 lewy';
$language['SET_PLAYER_CH3R'] = 'Kanał 3 prawy';
$language['SET_PLAYER_CH4L'] = 'Kanał 4 lewy';
$language['SET_PLAYER_CH4R'] = 'Kanał 4 prawy';
$language['SET_PLAYER_MSG_USE'] = 'Aktywuj na bramce MusicServera';
$language["SET_PLAYER_MSG_ZONENR"] = 'Numer strefy audio';
$language['SET_PLAYER_SQ2LOX_USE'] = 'Aktywuj Squeeze2Lox dla tej strefy';
$language['SET_PLAYER_SQ_VTITITLE'] = 'VTI Artysta/Tytuł';
$language['SET_PLAYER_SQ_VTIMODE'] = 'VTI Tryb';
$language['SET_PLAYER_SQ_VIVOLUME'] = 'VI Głośność';
$language['SET_PLAYER_SQ_VIPOWER'] = 'VI Zasilanie';
$language['SET_PLAYER_SQ_VTISYNC'] = 'VTI Sync';
$language['SET_PLAYER_PM_USE'] = 'Aktywuj PowerManager dla tej strefy';
$language['SET_PLAYER_PM_VIAMP'] = 'VI Wzmacniacz';
$language['SET_PLAYER_PM_URLON'] = 'URL - ON';
$language['SET_PLAYER_PM_URLOFF'] = 'URL - OFF';
$language['SET_PLAYER_MODAL_WRITE_TITLE'] = 'Zapisz ustawienia';
$language['SET_PLAYER_MODAL_WRITEOK_MSG'] = 'Ustawienia zostały poprawnie zapisane.<br>Odtwarzacz i wszystkie usługi zostały uruchomione ponownie.';
$language['SET_PLAYER_MODAL_WRITENOK_MSG'] = 'Ustawienia nie mogły zostać zapisane!';
$language['SET_PLAYER_MODAL_DEL_TITLE'] = 'Usuń strefę';
$language['SET_PLAYER_MODAL_DELOK_MSG'] = 'Strefa została popawnie usunięta.';
$language['SET_PLAYER_MODAL_DELNOK_MSG'] = 'Strefa nie mogła zostac usunięta.';
$language['SET_PLAYER_MODAL_DELASK_MSG'] = 'Czy na pewno chcesz usunąć strefę?';
$language['SET_PLAYER_REQ_NAME'] = 'Nazwa musi zostać podana. (A-Z, a-z, 0-9, _, -, spacje)';
$language['SET_PLAYER_REQ_ALSA'] = 'Parametry są wymagane. Std. jest (80:4::)';
$language['SET_PLAYER_REQ_SC'] = 'Ten kanał jest wymagany.';
$language['SET_PLAYER_REQ_URL'] = 'Wprowadź adres URL w poprawnym formacie. (http://xxxx.xx)';
$language['SET_PLAYER_TT_NAME'] = 'A-Z a-z 0-9 _ - spacje';
$language['SET_PLAYER_TT_ALSA'] = 'Dokonuj zmian tylko gdy pojawią się jakiekolwiek problemy.';
$language['SET_PLAYER_TT_FROMSYSTEM'] = 'Przypisany przez system.';
$language['SET_PLAYER_TT_CH'] = 'wybierz kanał.';
$language['SET_PLAYER_TT_CHOPT'] = 'wybierz opcjonalny kanał.';
$language['SET_PLAYER_TT_NUMBERSONLY'] = 'Wprowadź tylko cyfry.';
$language['SET_PLAYER_TT_URL'] = 'Format wprowadzania http://xxxx.xx';
$language['SET_PLAYER_TT_MSG_NUMBER'] = 'Wybierz numer strefy audio który używasz w Lox-Config dla tej strefy.';

//User
$language['SET_USER_PAGE_TITLE'] = 'Ustawienia użytkownika MS4L';
$language['SET_USER_TITLE'] = 'Ustawienia użytkownika';
$language['SET_USER_TITLE_USER'] = 'Użytkownik';
$language['SET_USER_NAME'] = 'Nazwa użytkownika';
$language['SET_USER_PASSWORD'] = 'Hasło';
$language['SET_USER_PASSWORD_CONFIRM'] = 'Potwierdź hasło';
$language['SET_USER_REQ_NAME'] = 'Nazwa musi zostać podana.. (A-Z, a-z, 0-9, _, -)';
$language['SET_USER_REQ_PASS'] = 'Hasło musi zostac podane. (A-Z, a-z, 0-9, _, ?, , +, -, ., :, -)';
$language['SET_USER_REQ_PASS_CONFIRM'] = 'Hasło musi być wprowadzone takie samo jak w polu poprzednim.';
$language['SET_USER_MODAL_WRITE_TITLE'] = 'Zapisz ustawienia';
$language['SET_USER_MODAL_WRITEOK_MSG'] = 'Ustawienia zostały zapisane poprawnie.';
$language['SET_USER_MODAL_WRITENOK_MSG'] = 'Ustawienia nie mogły zostać zapisane!';

//Equalizer
$language['SET_EQ_PAGE_TITLE'] = 'Equalizer MS4L';
$language["SET_EQ_TITLE"] = 'Equalizer';
$language["SET_EQ_TITLE_ZONES"] = 'Strefy';
$language["SET_EQ_LOAD_PRESET"] = 'Presety';
$language["SET_EQ_LOAD"] = 'Wczytaj preset użytkownika';
$language["SET_EQ_SAVE"] = 'Zapisz preset użytkownika';
$language["SET_EQ_LOAD_PRESET_FLAT"] = 'Flat';
$language["SET_EQ_LOAD_PRESET_ROCK"] = 'Rock';
$language["SET_EQ_LOAD_PRESET_LOUD"] = 'Loudness';
$language["SET_EQ_LOAD_PRESET_POP"] = 'Pop';
$language["SET_EQ_LOAD_PRESET_CLASSIC"] = 'Classic';
$language["SET_EQ_LOAD_USER1"] = '1';
$language["SET_EQ_SAVE_USER1"] = '1';
$language["SET_EQ_LOAD_USER2"] = '2';
$language["SET_EQ_SAVE_USER2"] = '2';
$language["SET_EQ_LOAD_GLOBAL"] = 'Global';
$language["SET_EQ_SAVE_GLOBAL"] = 'Global';
$language['SET_EQ_MODAL_WRITE_TITLE'] = 'Zapisz ustawienia';
$language['SET_EQ_MODAL_WRITEOK_MSG'] = 'Ustawienia zostały zapisane poprawnie.';
$language['SET_EQ_MODAL_WRITENOK_MSG'] = 'Ustawienia nie mogły zostać zapisane!';
$language["SET_EQ_MSG"] = 'Dla tej strefy jest włączony MusicServerGateway. Ustaw Equalizer w apce.';

//Setup Soundcard
$language['SET_SC_PAGE_TITLE'] = 'Ustawienia karty dźwiękowej MS4L';
$language["SET_SC_TITLE"] = 'Ustawienia karty dźwiękowej';
$language["SET_SC_TITLE_SC"] = 'Karty dźwiękowe';
$language["SET_SC_TITLE_SC_SYSTEM"] = 'Znaleziono karty dźwiękowe';
$language["SET_SC_SC1_USE"] = 'Aktywuj kartę dźwiękową 1';
$language["SET_SC_SC2_USE"] = 'Aktywuj kartę dźwiękową 2';
$language["SET_SC_SC3_USE"] = 'Aktywuj kartę dźwiękową 3';
$language["SET_SC_SC4_USE"] = 'Aktywuj kartę dźwiękową 4';
$language["SET_SC_SC5_USE"] = 'Aktywuj kartę dźwiękową 5';
$language["SET_SC_SC6_USE"] = 'Aktywuj kartę dźwiękową 6';
$language["SET_SC_SC7_USE"] = 'Aktywuj kartę dźwiękową 7';
$language["SET_SC_DEVICENR"] = 'Numer urządzenia';
$language["SET_SC_CHANNEL"] = 'Liczba kanałów';
$language["SET_SC_SAMPLING"] = 'Częstotliwość próbkowania';
$language['SET_SC_REQ_DEVCIENR'] = 'Numer urządzenia jest wymagany';
$language['SET_SC_REQ_CHANNEL'] = 'Liczba kanałów jest wymagana';
$language['SET_SC_REQ_SAMPLING'] = 'Częstotliwość próbkowania jest wymagana';
$language['SET_SC_MODAL_WRITE_TITLE'] = 'Zapisz ustawienia';
$language['SET_SC_MODAL_WRITEOK_MSG'] = 'Ustawienia zostały zapisane poprawnie.';
$language['SET_SC_MODAL_WRITENOK_MSG'] = 'Ustawienia nie mogły zostać zapisane!';

//Update
$language['UPDATE_PAGE_TITLE'] = 'Aktualizacja MS4L';
$language["UPDATE_TITLE"] = 'Aktualizacja';
$language["UPDATE_TITLE_MUSICSERVER"] = 'MusicServer';
$language["UPDATE_TITLE_LMS"] = 'Logitech Media Server';
$language["UPDATE_TITLE_SQUEEZE"] = 'Squeezelite';
$language["UPDATE_TITLE_SYSTEM"] = 'System';
$language["UPDATE_VERSION_ONLINE"] = 'Wersja online';
$language["UPDATE_VERSION_INSTALLED"] = 'Wersja zainstalowana';
$language["UPDATE_VERSION_PACKAGES"] = 'Pakiet(y)';
$language["UPDATE_BTN_UPDATE"] = 'Aktualizuj';
$language["UPDATE_BTN_LOG"] = 'Log (ostatnia aktualizacja)';
$language["UPDATE_BTN_RELOAD"] = 'Po ukazaniu się napisu AKTUALIZACJA ZAKOŃCZONA, możesz przeładować tę stronę tutaj';
$language["UPDATE_BTN_CHANGELOG"] = 'Historia zmian';
$language["UPDATE_MAILER_SUBJECT"] = 'Informacja o aktualizacjach MS4L';
$language["UPDATE_MAILER_TEXT"] = 'Cześć {realname},<br><br>Są nowości dla twojego serwera muzyki.<br><br>{verions}<br><br>Twój zespół MusicServer4Lox.';

//Backup
$language['BACKUP_PAGE_TITLE'] = 'Kopia zapasowa MS4L';
$language["BACKUP_TITLE"] = 'Kopia zapasowa';
$language["BACKUP_TITLE_MUSICSERVER"] = 'MusicServer';
$language["BACKUP_TITLE_MUSICSERVER_SETTINGS"] = 'Ustawienia MusicServera';
$language["BACKUP_TITLE_LMS"] = 'Logitech Media Server';
$language["BACKUP_TITLE_LMS_FAV"] = 'Ulubione Logitech Media Server';
$language["BACKUP_TITLE_LMS_PL"] = 'Playlisty Logitech Media Server';
$language["BACKUP_TITLE_MS_FS"] = 'Pliki i ustawienia MusicServera';
$language["BACKUP_BTN_CHOOSEFILE"] = 'Wybierz plik';
$language["BACKUP_BTN_BACKUP"] = 'Stwórz kopię zapasową';
$language["BACKUP_BTN_RESTORE"] = 'Przywróć';
$language["BACKUP_BTN_BACKUPLOG"] = 'Log (ostatnia kopia zapasowa)';
$language["BACKUP_BTN_RESTORELOG"] = 'Log (ostatnie przywracanie)';

//Reset Server
$language['RESET_SERVER_PAGE_TITLE'] = 'Resetowanie MS4L';
$language['RESET_SERVER_TITLE'] = 'Resetowanie';
$language["RESET_SERVER_TITLE_BOX"] = 'Zresetuj ustawienia serwera';
$language["RESET_SERVER_BTN"] = 'Zresetuj serwer';
$language["RESET_SERVER_MSG"] = '
<br>Po kliknięciu tego przycisku wszystkie ustawienia MS4L poza ustawieniami sieciowymi zostaną zresetowane.
<br>Logitech Media Server nie będzie zresetowany!
<br>Ta operacja może być odwrócona wyłącznie poprzez przywrócenie kopii zapasowej.';
$language["RESET_SERVER_MSG2"] = 'Ustawienia zostały zresetowane.<br>System zostanie automatycznie uruchomiony ponownie po zresetowaniu...';
$language["RESET_SERVER_MODAL_TITLE"] = 'Resetowanie ustawień serwera';
$language["RESET_SERVER_MODAL_SHUTDOWN_MSG"] = 'Na pewno chcesz zresetować serwer?';

//SupportData
$language['SUPPORT_PAGE_TITLE'] = 'Dane diagnostyczne MS4L';
$language['SUPPORT_TITLE'] = 'Dane diagnostyczne';
$language["SUPPORT_TITLE_BOX"] = 'Dane diagnostyczne';
$language["SUPPORT_DOWNLOAD_BTN"] = 'Pobierz plik z danymi diagnostycznymi niezbędny do uzyskania wsparcia na Loxforum';

//Template Generator
$language['TEMPLATE_PAGE_TITLE'] = 'Kreator szablonu MS4L';
$language['TEMPLATE_TITLE'] = 'Kreator szablonu';
$language["TEMPLATE_TITLE_BOX"] = 'Pobierz';
$language["TEMPLATE_DOWNLOAD_CLI"] = 'Szablon CLI';
$language["TEMPLATE_DOWNLOAD_T5"] = 'Szablon T5';
$language["TEMPLATE_DOWNLOAD_TS"] = 'Szablon T5 (alternatywna obsługa T5 (Labmaster)';

//Services
$language['SET_SERVICE'] = 'Usługi';
$language['SET_SERVICE_PM'] = 'Powermanager';
$language['SET_SERVICE_SQ2LOX'] = 'Squeeze2lox / MSG';
$language['SET_SERVICE_FC'] = 'FritzCall-Monitor';
$language['SET_SERVICE_T5'] = 'Odbiornik T5';
$language['SET_SERVICE_FM'] = 'FollowMe';
$language['SET_SERVICE_MODAL_WRITE_TITLE'] = 'Zapisz ustawienia';
$language['SET_SERVICE_MODAL_WRITEOK_MSG'] = 'Ustawienia zostały zapisane poprawnie.';
$language['SET_SERVICE_MODAL_WRITENOK_MSG'] = 'Ustawienia nie mogły zostać zapisane!';

//PowerManager
$language['SET_PM_PAGE_TITLE'] = 'Usługa PoawerManagera MS4L';
$language["SET_PM_TITLE"] = 'Ustawienia PowerManagera';
$language["SET_PM_TITLE_ZONE"] = 'PowerManager';
$language["SET_PM_TITLE_AMP"] = 'Amp';
$language["SET_PM_TITLE_VOLUMERESET"] = 'Reset głośności';
$language['SET_PM_AUTOSTART'] = 'Aktywuj automatyczny start';
$language['SET_PM_POWEROFF_STOP'] = 'Wyłącz strefę po Stop';
$language['SET_PM_POWEROFF_PAUSE'] = 'Wyłącz strefę po Pauzie';
$language['SET_PM_AMPALL'] = 'Aktywuj zarządzanie wzmacniaczem dla wszystkich stref';
$language['SET_PM_AMPOFF'] = 'Czas wyłączenia wzmacniacza';
$language['SET_PM_AMPSTARTUP'] = 'Czas włączenia wzmacniacza';
$language['SET_PM_VIAMPALL'] = 'VI wzmacniacza';
$language['SET_PM_URLALLON'] = 'URL - On';
$language['SET_PM_URLALLOFF'] = 'URL - Off';
$language["SET_PM_VOLUMERESET"] = 'Aktywuj reset głośności dla wewnętrznych stref';
$language["SET_PM_STARTVOLUME"]  = 'Wartość głośności dla wewnętrznych stref';
$language["SET_PM_VOLUMERESET_EXT"] = 'Aktywuj reset głośności dla zewnętrznych stref';
$language["SET_PM_STARTVOLUME_EXT"]  = 'Wartość głośności dla zewnętrznych stref';
$language["SET_PM_REQ_POWEROFF"] = 'Tutaj musi być wprowadzony czas większy niż 10 sekund.';
$language["SET_PM_REQ_AMPSTARTUP"] = 'Tutaj musi być wprowadzona wartość, nawet w przypadku 0 sekund.';
$language["SET_PM_REQ_AMPVIAMPALL"] = 'Tutaj musi być wprowadzona wartość, 0 jeśli tylko URL ma przełączać.';
$language["SET_PM_REQ_STARTVOLUME"] = 'Tutaj musi być wprowadzona wartość początkowej głośności, większa niż 0.';
$language["SET_PM_REQ_AMPURL"] = 'Wprowadź adres URL w poprawnym formacie. (http://xxxx.xx)';
$language['SET_PM_TT_NUMBERSONLY'] = 'Wprowadź tylko cyfry.';
$language['SET_PM_TT_URL'] = 'Format wprowadzania http://xxxx.xx';;

//Squeezw2Lox
$language['SET_SQ2LOX_PAGE_TITLE'] = 'Usługa Squeeze2Lox / MSG MS4L';
$language["SET_SQ2LOX_TITLE"] = 'Usługa Squeeze2Lox / MSG';
$language["SET_SQ2LOX_TITLE_BOX"] = 'Squeeze2Lox / MusicServerGateway';
$language['SET_SQ2LOX_AUTOSTART'] = 'Aktywuj automatyczny start';

//T5 Receiver
$language['SET_T5_PAGE_TITLE'] = 'Usługa odbiornika T5 MS4L';
$language["SET_T5_TITLE"] = 'Ustawienia odbiornika T5';
$language["SET_T5_TITLE_BOX"] = 'Odbiornik T5';
$language['SET_T5_AUTOSTART'] = 'Aktywuj automatyczny start';
$language['SET_T5_UDP'] = 'Port UDP';
$language['SET_T5_SAYFAV'] = 'Say Favorite';
$language['SET_T5_SAYFAVVOL'] = 'Say Favorite podbicie głośności';
$language['SET_T5_VOLUMESTEP'] = 'Stopień głośności';
$language["SET_T5_REQ_UDP"] = 'Wartość musi być z zakresu 1-65535';
$language["SET_T5_TT_UDP"] = 'Port UDP przez który Miniserver wysyła dane do MS4L';
$language["SET_T5_TT_VOLUMESTEP"] = 'Liczba stopni o które zmienia się głośność (zwiększa/zmniejsza) przy każdym kliknięciu.';
$language["SET_T5_TT_SAYFAVVOL"] = 'Głośność jest zwiększana o tę wartość gdy ulubione są rozgłaszane.';

//FollowMe
$language['SET_FM_PAGE_TITLE'] = 'Usługa FollowMe MS4L';
$language["SET_FM_TITLE"] = 'Ustawienia FollowMe';
$language["SET_FM_TITLE_FM"] = 'FollowMe';
$language["SET_FM_STOPFROM"] = 'Stop From-Zone';
$language["SET_FM_PLAYTO"] = 'Play To-Zone';
$language["SET_FM_VOLUMETO"] = 'Głośność From-Zone->To-Zone';
$language["SET_FM_TT_STOPFROM"] = 'Ustaw aby we From-Zone muzyka automatycznie się zatrzymała.';
$language["SET_FM_TT_PLAYTO"] = 'Ustaw aby w To-Zone muzyka automatycznie była odtwarzana.';
$language["SET_FM_TT_VOLUMETO"] = 'Ustaw ten tę samą głośność From-Zone->To-Zone.';

//FritzCallMonitor
$language['SET_FC_PAGE_TITLE'] = 'Usługa CallMonitor MS4L';
$language["SET_FC_TITLE"] = 'Ustawienia CallMonitor';
$language["SET_FC_TITLE_FC"] = 'CallMonitor';
$language["SET_FC_TITLE_TTS"]= 'Text2Speech';
$language["SET_FC_TITLE_NUMBERS"]= 'Numer / nazwa';
$language["SET_FC_CMOK"] = 'CallMonitor jest osiągalny.';
$language["SET_FC_CMNOK"] = 'CallMonitor nie jest osiągalny. Aktywowany?';
$language["SET_FC_CMIPEMPTY"] = 'Adres IP Fitzboxa nie został ustawiony!';
$language["SET_FC_AUTOSTART"]= 'Aktywuj automatyczny start';
$language["SET_FC_IP"]= 'Ares IP Fritzboxa';
$language["SET_FC_USER"]= 'Nazwa użytkownika Fritzboxa';
$language["SET_FC_PASS"]= 'Hasło Fritzboxa';
$language["SET_FC_TTSVI"] = 'Numer VI - impuls startowy';
$language["SET_FC_CALLERVTI"]  = 'Numer VTI - Dzwoniący';
$language["SET_FC_CALLEDVTI"] = 'Numer VTI - Wydzwaniany';
$language["SET_FC_NUMBERTALKSPEED"] = 'Odczytuj numery telefonów powoli.';
$language["SET_FC_NUMBER"] = 'Numer telefonu';
$language["SET_FC_NAME"] = 'Nazwa linii telefonicznej';
$language["SET_FC_REQ_IP"] = 'Adres IP musi być wprowadzony. (xxx.xxx.xxx.xxx)';
$language["SET_FC_REQ_USER"] = 'Nazwa użytkownika musi być wprowadzona. (A-Z, a-z, 0-9, _, -)';
$language["SET_FC_REQ_PASS"] = 'Hasło musi być wprowadzone. (A-Z, a-z, 0-9, _, ?, , +, -, ., :, -)';
$language["SET_FC_REQ_TTSVI"] = 'VI dla impulsu musi być wprowadzone. (tylko cyfry)';
$language["SET_FC_REQ_CALLERVTI"] = 'VTI Dzwoniącego musi być wprowadzony. (tylko cyfry)';
$language["SET_FC_REQ_CALLEDVTI"] = 'VTI Wydzwanianego musi być wprowadzony. (tylko cyfry)';
$language["SET_FC_REQ_NUMBER"] = 'Wprowadź numer telefonu';
$language["SET_FC_REQ_NAME"] = 'Wprowadź nazwę, (A-Z, a-z, 0-9, _, -)';

//Network
$language["SET_NETWORK"] = 'Sieć';
$language["SET_NETWORK_MOUNT"]  = 'Podłączanie udziału sieciowego';
$language['SET_NET_PAGE_TITLE'] = 'Ustawienia sieciowe MS4L';
$language["SET_NET_TITLE"] = 'Ustawienia sieciowe';
$language["SET_NET_TITLE_NET"] = 'Sieć';
$language["SET_NET_TITLE_HOSTNAME"] = 'Nazwa hosta';
$language["SET_NET_TITLE_IFACE"] = 'Interfejs sieciowy';
$language["SET_NET_IFACENAME"] = 'Nazwa interfejsu';
$language["SET_NET_MACADDRESS"] = 'Adres mac';
$language["SET_NET_HOSTNAME"] = 'Nazwa hosta';
$language["SET_NET_HOSTDOMAIN"] = 'rozszerzenie nazwy hosta';
$language["SET_NET_DHCP"] = 'Używaj DHCP';
$language["SET_NET_IP"] = 'Adres IP';
$language["SET_NET_SUBNET"] = 'Podsieć';
$language["SET_NET_GATEWAY"] = 'Brama';
$language["SET_NET_DNSSERVER"] = 'Serwer DNS';
$language['SET_NET_MODAL_WRITE_TITLE'] = 'Zapisz ustawienia';
$language['SET_NET_MODAL_WRITEOK_MSG'] = 'Ustawienia zostały zapisane poprawnie.';
$language['SET_NET_MODAL_WRITEOK_MSG1'] = 'Aby nowe ustawienia zostały aktywowane system musi zostać uruchomiony ponownie.';
$language['SET_NET_MODAL_WRITEOK_MSG2'] = 'Czy chciałbyś system uruchomić ponownie teraz?';
$language['SET_NET_MODAL_WRITENOK_MSG'] = 'Ustawienia nie mogły zostać zapisane!';

//NetworkShare
$language['SET_NETSHARE_PAGE_TITLE'] = 'Ustawienia dysku sieciowego MS4L'; 
$language["SET_NETSHARE_TITLE"] = 'Podłączanie dysku sieciowego';
$language["SET_NET_TITLE_NETSHARE"] = 'Ustawienia systemu NAS';
$language["SET_NET_AUTOMOUNT"] = 'Montuj dysk sieciowy automatycznie';
$language["SET_NET_IPHOSTNAME"] = 'Adres IP / nazwa hosta NAS';
$language["SET_NET_FOLDER"] = 'Nazwa folderu NAS';
$language["SET_NET_TITLE_AUTH"] = 'Dane logowania do NAS';
$language["SET_NET_LOGIN"] = 'Aktywuj logowanie';
$language["SET_NET_USER"] = 'Nazwa użytkownika';
$language["SET_NET_PASS"] = 'Hasło';
$language["SET_NET_CHARTSET"] = 'Użyj kodowanie znaków utf8 zamiast iso8859-1';
$language["SET_NET_FOLDERNAS"] = 'Nazwa folderu NAS';
$language["SET_NET_REQ_IPHOSTNAME"] = 'Adres IP lub nazwa hosta musi być wprowadzona (bez //)';
$language["SET_NET_REQ_FOLDER"]  = 'Nazwa folderu musi być wprowadzona (bez /)';
$language["SET_NET_REQ_USER"] = 'Nazwa użytkownika musi być wprowadzona. (A-Z, a-z, 0-9, _, -)';
$language["SET_NET_REQ_PASS"] = 'Hasło musi być wprowadzone. (A-Z, a-z, 0-9, _, ?, , +, -, ., :, -)';
$language["SET_NET_BTN_MOUNT"] = 'Zamontuj dysk';
$language["SET_NET_BTN_UNMOUNT"] = 'Odmontuj dysk';
$language["SET_NET_MOUNTOK"] = 'Dysk sieciowy jest zamontowany';
$language["SET_NET_MOUNTNOK"]  = 'Dysk sieciowy nie jest zamontowany';

//Mail
$language['SET_MAIL_PAGE_TITLE'] = 'Ustawienia eMail MS4L'; 
$language["SET_MAIL_TITLE"] = 'Ustawienia eMail';
$language["SET_MAIL_TITLE_NAME"] = 'eMail / Nazwa`';
$language["SET_MAIL_TITLE_SMTP"] = 'Serwer SMTP';
$language["SET_MAIL_TITLE_UPDATEMAIL"] = 'Powiadomienia mailowe o aktualizacjach';
$language["SET_MAIL_REALNAME"] = "Rzeczywista nazwa";
$language["SET_MAIL_EMAILTO"] = "Adres eMail - odbiorca";
$language["SET_MAIL_SMTPSERVER"] = 'Serwer SMTP';
$language["SET_MAIL_SMTPPORT"] = 'Port SMTP';
$language["SET_MAIL_SMTPSECURE"] = 'Zabezpieczenie SMTP';
$language["SET_MAIL_SMTPUSER"] = 'Nazwa użytkownika SMTP';
$language["SET_MAIL_SMTPPASS"] = 'Hasło SMTP';
$language["SET_MAIL_UPDATEMAIL"] = 'Aktywuj powiadomienia o aktualizacjach';
$language["SET_MAIL_UPDATEINTERVAL"] = 'Interwał powiadomień o aktualizacjach';
$language["SET_MAIL_UPDATEDAY"] = 'Dzień powiadomień o aktualizacjach';
$language["SET_MAIL_UPDATEWEEKLY"] = 'tygodniowy';
$language["SET_MAIL_UPDATEDAYLY"] = 'dzienny';
$language["SET_MAIL_UPDATEMO"] = 'Poniedziałek';
$language["SET_MAIL_UPDATETU"] = 'Wtorek';
$language["SET_MAIL_UPDATEWE"] = 'Środa';
$language["SET_MAIL_UPDATETH"] = 'Czwartek';
$language["SET_MAIL_UPDATEFR"] = 'Piątek';
$language["SET_MAIL_UPDATESA"] = 'Sobota';
$language["SET_MAIL_UPDATESU"] = 'Niedziela';
$language["SET_MAIL_REQ_REALNAME"] = "Nazwa musi być wprowadzona";
$language["SET_MAIL_REQ_EMAILTO"] = "Adres eMail musi być wprowadzony";
$language["SET_MAIL_REQ_SMTPSERVER"] = 'Serwer SMTP musi być wprowadzony';
$language["SET_MAIL_REQ_SMTPPORT"] = 'Port SMTP musi być wprowadzony (1-65535)';
$language["SET_MAIL_REQ_SMTPSECURE"] = 'Zabezpieczenie SMTP musi być wprowadzone';
$language["SET_MAIL_REQ_SMTPUSER"] = 'Nazwa użytkownika STMP musi być wprowadzona';
$language["SET_MAIL_REQ_SMTPPASS"] = 'Hasło STMP musi być wprowadzone';
$language["SET_MAIL_BTN_TESTMAIL"] = 'Wyślij testowy eMail';
$language["SET_MAIL_TESTOK"] = 'eMail został poprawnie wysłany';
$language["SET_MAIL_TESTNOK"] = 'eMail nie mógł zostać wysłany.'; 

//MiniServer
$language['SET_MS_PAGE_TITLE'] = 'Ustawienia MiniServera MS4L';
$language['SET_MS_TITLE'] = 'Ustawienia MiniServera';
$language['SET_MS_TITLE_MS'] = 'MiniServer';
$language['SET_MS_IP'] = 'Adres IP MiniServera';
$language['SET_MS_USER'] = 'Nazwa użytkownika MiniServera';
$language['SET_MS_PASSWORD'] = 'Hasło MiniServera';
$language['SET_MS_PASSWORD_CONFIRM'] = 'Potwierdzenia hasła MiniServera';
$language['SET_MS_REQ_IP'] = 'Adres IP musi być wprowadzony. (xxx.xxx.xxx.xxx)';
$language['SET_MS_REQ_USER'] = 'Nazwa użytkownika musi być wprowadzona. (A-Z, a-z, 0-9, _, -)';
$language['SET_MS_REQ_PASS'] = 'Hasło musi być wprowadzone. (A-Z, a-z, 0-9, _, ?, , +, -, ., :, -)';
$language['SET_MS_REQ_PASS_CONFIRM'] = 'Hasło musi być wprowadzone takie samo jak w polu poprzednim.';

//LigitechMediaServer
$language['SET_LMS_PAGE_TITLE'] = 'Ustawienia Logitech Media Servera MS4L';
$language['SET_LMS_TITLE'] = 'Ustawienia Logitech Media Servera';
$language['SET_LMS_TITLE_LMS'] = 'Logitech Media Server';
$language['SET_LMS_IP'] = 'Adres IP LMS (ustawiony przez system)';
$language['SET_LMS_WEBPORT'] = 'Port WebUI LMS';
$language['SET_LMS_CLIPORT'] = 'Port CLU LMS';
$language['SET_LMS_8'] = 'Użyj LMS w wersji 8 (Beta)';
$language['SET_LMS_REQ_WEBPORT'] = 'Port WebUI musi być z zakresu 1-65535';
$language['SET_LMS_REQ_CLIPORT'] = 'Port CLI musi być z zakresu 1-65535';

//Statisics
$language['SET_STAT_PAGE_TITLE'] = 'Ustawienia statystyk MS4L';
$language['SET_STAT_TITLE'] = 'Ustawienia statystyk';
$language['SET_STAT_TITLE_MS'] = 'Statystyki';
$language['SET_STAT_SEND'] = 'Wysyłaj statystyki';
$language['SET_STAT_SHOW'] = 'Pokaż statystyki';
$language['SET_STAT_UUID'] = 'UUID (Universally Unique IDentifier)';

//TTS
$language["SET_TTS_TTS"] = 'TTS / CTS';
$language["SET_TTS_RING"] = 'Dzwonek/alarm/budzik';
$language["SET_TTS_WEATHER"] = 'Weather2TTS';
$language["SET_TTS_TRAVEL"] = 'TravelTime2TTS';
$language['SET_TTS_PAGE_TITLE'] = 'Ustawienia MS4L TTS / dzwonka';
$language['SET_TTS_TITLE'] = 'Ustawienia Text2Speech / dzwonka';
$language["SET_TTS_TITLE_CACHE"] = 'TTS Cache';
$language["SET_TTS_TITLE_RING"] = 'Ustawienia domyślne dzwonka / alarmu / budzika';
$language['SET_TTS_TITLE_DEFAULT'] = 'Ustawienia domyślne TTS';
$language['SET_TTS_DEFAULT_PROVIDER'] = 'Dostawca';
$language['SET_TTS_DEFAULT_VOLUME'] = 'Głośność';
$language["SET_TTS_DEFAULT_AUTOPLAY"] = 'AutoPlay po zdarzeniu';
$language["SET_TTS_DEFAULT_AUTOSYNC"] = 'AutoSync po zdarzeniu';
$language['SET_TTS_DEFAULT_OVERLAY'] = 'Aktywacja Overlay';
$language['SET_TTS_DEFAULT_OVERLAYDROP'] = 'Overlay-Drop';
$language['SET_TTS_DEFAULT_VOLUME'] = 'Głośność';
$language['SET_TTS_DEFAULT_TIMEOUT'] = 'Timeout';
$language["SET_TTS_DEFAULT_RING_FILE"] = 'Dźwięk dzwonka';
$language["SET_TTS_DEFAULT_DELETE_FILEAFTER"] = 'Usuń nieużywane pliki TTS po (dniach)';
$language["SET_TTS_DEFAULT_TTSCACHE"] = 'Pamięć podręczna (cache) TTS';
$language["SET_TTS_DEFAULT_TTSCACHEFILE"] = 'Plik(i)';
$language["SET_TTS_DEFAULT_CLEARCACHE"] = 'Usuń pamięć podręczną (cache) TTS';
$language['SET_TTS_TITLE_POLLY'] = 'Polly';
$language['SET_TTS_TITLE_VOICERSS'] = 'VoiceRSS';
$language['SET_TTS_TITLE_RESPONSIVEVOICE'] = 'Responsive Voice';
$language["SET_TTS_POLLY_ACCESSID"] = 'Access Key ID';
$language["SET_TTS_POLLY_SECRETKEY"] = 'Secret Access Key';
$language["SET_TTS_POLLY_LANG"] = 'Język / głos';
$language["SET_TTS_VOICERSS_TOKEN"] = 'Token';
$language["SET_TTS_VOICERSS_QUALITY"] = 'Jakość audio';
$language["SET_TTS_VOICERSS_LANG"] = 'Język / głos';
$language["SET_TTS_REQ_DEFAULT_VOLUME"] = 'Głośność musi być z zakresu 1-100.';
$language["SET_TTS_REQ_DEFAULT_TIMEOUT"] = 'Timeout musi być z zakresu 10-360.';;
$language["SET_TTS_REQ_DEFAULT_DELETE_FILEAFTER"] = 'Liczba dni musi być z zakresu 0-365.';

//Weather2TTS
$language['SET_W2T_PAGE_TITLE'] = 'Ustawienia Weather2TTS MS4L';
$language["SET_W2T_TITLE"] = 'Ustawienia Weather2TTS';
$language["SET_W2T_TITLE_BASIC"] = 'Ustawienia podstawowe';
$language["SET_W2T_TITLE_DEFAULT"] = 'Ustawienia domyślne';
$language["SET_W2T_TITLE_TEXT"] = 'Teksty';
$language["SET_W2T_DEFAULT_APIKEY"] = 'API-Key - Weatherbit.io';
$language["SET_W2T_DEFAULT_LAT"] = 'Szerokość geograficzna (Latitude)';
$language["SET_W2T_DEFAULT_LON"] = 'Długość geograficzna (Longitude)';
$language["SET_W2T_DEFAULT_FINDGEO"] = 'Znajdź dane geograficzne';
$language["SET_W2T_DEFAULT_STANDARD_OUT"] = 'Wyjście'; 
$language["SET_W2T_DEFAULT_RAIN_THRESHOLD"] = 'Próg wiatru';
$language["SET_W2T_DEFAULT_WIND_THRESHOLD"] = 'Próg deszczu';
$language["SET_W2T_DEFAULT_TEXT"] = 'Edycja tekstu';
$language["SET_W2T_DEFAULT_TEXTCHOOSE"] = 'Wybierz tekst';
$language["SET_W2T_DEFAULT_OVERWRITE"] = 'Nadpisz tekst domyślnym dla języka';
$language["SET_W2T_TEXT_RAIN"] = 'Deszcz (today)';
$language["SET_W2T_TEXT_WIND"] = 'Wiatr (today)';
$language["SET_W2T_TEXT_RAINFC"] = 'Deszcz (forecast)';
$language["SET_W2T_TEXT_WINDFC"] = 'Wiatr(forecast)';
$language["SET_W2T_TEXT_NOW"] = 'teraz (today)';
$language["SET_W2T_TEXT_TODAY"] = 'dzisiaj (today)';
$language["SET_W2T_TEXT_TODAY22"] = 'dzisiaj po godzinie 22 (today)';
$language["SET_W2T_TEXT_TODAYTOMORROW"] = 'dzisiaj i jutro (today_forecast)';
$language["SET_W2T_TEXT_FORECAST"] = 'jutro (forecast)';
$language["SET_W2T_TEXT_DAYTIME6_11"] = 'w ciągu dnia w godzinach 6-11 (daytime)';
$language["SET_W2T_TEXT_DAYTIME11_17"] = 'w ciągu dnia w godzinach 11-17 (daytime)';
$language["SET_W2T_TEXT_DAYTIME17_22"] = 'w ciągu dnia w godzinach 17-22 (daytime)';
$language["SET_W2T_TEXT_DAYTIME22_6"] = 'w ciągu dnia w godzinach 22-6 (daytime)';
$language["SET_W2T_TEXT_CUSTOM1"] = 'custom 1 (custom_1)';
$language["SET_W2T_TEXT_CUSTOM2"] = 'custom 2 (custom_2)';
$language["SET_W2T_TEXT_ASTRONOMIC"] = 'Astro (astronomic)';
$language["SET_W2T_TEXT_SUNSET"] = 'Zachód i wschód słońca (sunset)';
$language["SET_W2T_TEXT_MOONPHASE"] = 'fazy księżyca (moonphase)';
$language["SET_W2T_REQ_DEFAULT_APIKEY"] = 'API musi być skonfigurowane';
$language["SET_W2T_REQ_DEFAULT_LAT"] = 'Szerokość geograficzna musi być w pomiędzy -90 i 180°';
$language["SET_W2T_REQ_DEFAULT_LON"] = 'Długość geograficzna musi być pomiędzy -90 i 180°';
$language["SET_W2T_REQ_DEFAULT_RAIN_THRESHOLD"] = 'Próg deszczu musi być w zakresie 0-100%.';
$language["SET_W2T_REQ_DEFAULT_WIND_THRESHOLD"] = 'Próg wiatru musi być w zakresie 0-100%.';
//Weather2TTS Standard-Texts
//These Texts can be load as standards in WebUi, and changes can be made in the WebUI. Changes will be saved in the Config-File 
//All words between {} are variables and may not be modified!!!
$language["WTTS_TEXT_RAIN_TODAY"] = 'Prawdopodobieństwo deszczu wynosi {chance_of_rain_today} procent.';
$language["WTTS_TEXT_WIND_TODAY"] = 'Wieje {wind_text_today} z kierunku {wind_direction_today} z prędkością wynoszącą do {wind_speed_today} km/h.';
$language["WTTS_TEXT_RAIN_TOMORROW"] = 'Prawdopodobieństwo deszczu wynosi {chance_of_rain_tomorrow} procent.';
$language["WTTS_TEXT_WIND_TOMORROW"] = 'Wieje {wind_text_tomorrow} z kierunku {wind_direction_tomorrow} z prędkością wynoszącą do {wind_speed_tomorrow} km/h.';
$language["WTTS_TEXT_NOW"] = 'Aktualnie {condition_now}, przy {temp_now} stopni. Wilgotność wynosi {relative_humidity_now} procent, ciśnienie powietrza {pressure_now} milibarów. Wiatr wieje z kierunku {wind_direction_now} z prędkością {wind_speed_now} Km/h ze sporadycznymi porywami do {wind_gust_now} Km/h. Ilość deszczu wynosi {rain_fall_now} milimetrów na metr kwadratowy. Promieniowanie UV jest {uv_radiation_now}.';
$language["WTTS_TEXT_TODAY"] = 'Dzisiaj {condition_today}, najwyższa temperatura wyniesie {temp_high_today} stopni, najniższa temperatura wyniesie {temp_low_today} stopni. {rain_text_today}{wind_text_today}';
$language["WTTS_TEXT_TODAY2"] = 'Aktualnie {condition_now}, aktualna temperatura wynosi {temp_now} stopni. Jutro {condition_tomorrow}, temperatura będzie wynosić od {temp_low_tomorrow} do {temp_high_tomorrow} stopni. {rain_text_tomorrow} {wind_text_tomorrow}';
$language["WTTS_TEXT_TODAY_FORECAST"] = 'Dzisiaj {condition_today}, najwyższa temperatura wyniesie {temp_high_today} stopni, najniższa temperatura wyniesie {temp_low_today} stopni. {rain_text_today} {wind_text_today}. Jutro {condition_tomorrow}, temperatura będzie wynosić od {temp_low_tomorrow} do {temp_high_tomorrow} stopni. {rain_text_tomorrow} {wind_text_tomorrow}';
$language["WTTS_TEXT_FORECAST"] = 'Jutro {condition_tomorrow}, temperatura będzie wynosić od {temp_low_tomorrow} do {temp_high_tomorrow} stopni. {rain_text_tomorrow} {wind_text_tomorrow}';
$language["WTTS_TEXT_DAYTIME_6_11"] = 'Rano {condition_now}, spodziewana najwyższa temperatura wyniesie {temp_high_today} stopni, aktualna temperatura wynosi {temp_now} stopni. {rain_text_today} {wind_text_today}';
$language["WTTS_TEXT_DAYTIME_11_17"] = 'Po południu {condition_now}. Aktualna temperatura na zewnątrz wynosi {temp_now} stopni. {rain_text_today} {wind_text_today}';
$language["WTTS_TEXT_DAYTIME_17_22"] = 'Wieczorem {condition_now}. Aktualna temperatura na zewnątrz wynosi {temp_now} stopni, najniższa spodziewana temperatura wyniesie {temp_low_today} stopni. {rain_text_today} {wind_text_today}';
$language["WTTS_TEXT_DAYTIME_22"] = 'W tej chwili jest {temp_now} stopni. Jutro {condition_tomorrow}, temperatura wyniesie będzie wynosić od {temp_low_tomorrow} do {temp_high_tomorrow} stopni. {rain_text_tomorrow} {wind_text_tomorrow}';
$language["WTTS_TEXT_CUSTOM_1"] = 'Twój tekst 1';
$language["WTTS_TEXT_CUSTOM_2"] = 'Twoj tekst 2';
$language["WTTS_TEXT_ASTRO"] = 'Wschód słońca dzisiaj o {sunriseh}:{sunrisem}, zachód o {sunseth}:sunsetm}, aktualna faza księżyca to {moonphase}, iluminacja wynosi {moonillu}%.';
$language["WTTS_TEXT_SUNSET"] = 'Wschód słońca dzisiaj jest o {sunriseh}:{sunrisem}, a zachód o {sunseth:{sunsetm}.';
$language["WTTS_TEXT_MOONPHASE"] = 'Aktualna faza księżyca to {moonphase}, aktualna iluminacja wynosi {moonillu}%.';

$language["WTTS_CONDITION_200"]	= 'spodziewana jest burza z lekkim deszczem';
$language["WTTS_CONDITION_201"]	= 'spodziewana jest burza z deszczem';
$language["WTTS_CONDITION_202"]	= 'spodziewana jest burza z obfitym deszczem';
$language["WTTS_CONDITION_230"]	= 'spodziewana jest burza z lekką mżawką';
$language["WTTS_CONDITION_231"]	= 'spodziewana jest burza z mżawką';
$language["WTTS_CONDITION_232"]	= 'spodziewana jest burza z obfitą mżawką';
$language["WTTS_CONDITION_233"]	= 'spodziewana jest burza z opadami gradu';
$language["WTTS_CONDITION_300"]	= 'spodziewana jest lekka mżawka';
$language["WTTS_CONDITION_301"]	= 'spodziewana jest mżawka';
$language["WTTS_CONDITION_302"]	= 'spodziewana jest obfita mżawka';
$language["WTTS_CONDITION_500"]	= 'spodziewane są lekkie opady deszczu';
$language["WTTS_CONDITION_501"]	= 'spodziewane są umiarkowane opady deszczu';
$language["WTTS_CONDITION_502"]	= 'spodziewane są obfite opady deszczu';
$language["WTTS_CONDITION_511"]	= 'spodziewany jest marznący deszcz';
$language["WTTS_CONDITION_520"]	= 'spodziewane są lekkie opady przelotne';
$language["WTTS_CONDITION_521"]	= 'spodziewane są przelotne opady';
$language["WTTS_CONDITION_522"]	= 'spodziewane są obfite opady przelotne';
$language["WTTS_CONDITION_600"]	= 'spodziewane są lekkie opady śniegu';
$language["WTTS_CONDITION_601"]	= 'spodziewane są opady śniegu';
$language["WTTS_CONDITION_602"]	= 'spodziewane są obfite opady śniegu';
$language["WTTS_CONDITION_610"]	= 'spodziewane są opady deszczu ze śniegiem';
$language["WTTS_CONDITION_611"]	= 'spodziewana jest gołoledź';
$language["WTTS_CONDITION_612"]	= 'spodziewana jest duża gołoledź';
$language["WTTS_CONDITION_621"]	= 'spodziewane są przelotne opady śniegu';
$language["WTTS_CONDITION_622"]	= 'spodziewane są obfite przelotne opady śniegu';
$language["WTTS_CONDITION_623"]	= 'spodziewana jest ulewa';
$language["WTTS_CONDITION_700"]	= 'spodziewane jest zamglenie';
$language["WTTS_CONDITION_711"]	= 'spodziewane jest zamglenie';
$language["WTTS_CONDITION_721"]	= 'spodziewana jest mgła';
$language["WTTS_CONDITION_731"]	= 'spodziewane jest zapylenie';
$language["WTTS_CONDITION_741"]	= 'spodziewana jest mgła';
$language["WTTS_CONDITION_751"]	= 'spodziewana jest marznąca mgła';
$language["WTTS_CONDITION_800"]	= 'spodziewane jest czyste niebo';
$language["WTTS_CONDITION_801"]	= 'spodziewane jest nieznaczne zachmurzenie';
$language["WTTS_CONDITION_802"]	= 'spodziewane są rozproszone chmury';
$language["WTTS_CONDITION_803"]	= 'spodziewane jest częściowe zachmurzenie';
$language["WTTS_CONDITION_804"]	= 'spodziewane jest zachmurzenie';
$language["WTTS_CONDITION_900"]	= 'spodziewane są bliżej nieokreślone opady';

$language["WTTS_DIRECTION_UKNOWN"] = 'nieznany';
$language["WTTS_DIRECTION_E"] = 'Wschód';
$language["WTTS_DIRECTION_ENE"] = 'Wschodni lekko północny';
$language["WTTS_DIRECTION_ESE"] = 'Wschodni lekko południowy';
$language["WTTS_DIRECTION_N"] = 'Północ';
$language["WTTS_DIRECTION_NE"] = 'Północny wschód';
$language["WTTS_DIRECTION_NNE"] = 'Północny lekko wschodni';
$language["WTTS_DIRECTION_NNW"] = 'Północny lekko zachodni';
$language["WTTS_DIRECTION_NW"] = 'Północny zachód';
$language["WTTS_DIRECTION_S"] = 'Południe';
$language["WTTS_DIRECTION_SE"] = 'Południowy wschód';
$language["WTTS_DIRECTION_SSE"] = 'Południowy lekko wschodni';
$language["WTTS_DIRECTION_SSW"] = 'Południowy lekko zachodni';
$language["WTTS_DIRECTION_SW"] = 'Południowy zachód';
$language["WTTS_DIRECTION_W"] = 'Zachód';
$language["WTTS_DIRECTION_WNW"] = 'Zachodni lekko północny';
$language["WTTS_DIRECTION_WSW"] = 'Zachodni lekko południowy';
//Beaufort scale
$language["WTTS_WIND_1"] = 'lekki powiew wiatru';
$language["WTTS_WIND_2"] = 'słaby wiatr';
$language["WTTS_WIND_3"] = 'łagodny wiatr';
$language["WTTS_WIND_4"] = 'umiarkowany wiatr';
$language["WTTS_WIND_5"] = 'dość silny wiatr';
$language["WTTS_WIND_6"] = 'silny wiatr';
$language["WTTS_WIND_7"] = 'bardzo silny wiatr';
$language["WTTS_WIND_8"] = 'gwałtowny wiatr';
$language["WTTS_WIND_9"] = 'wichura';
$language["WTTS_WIND_10"] = 'silna wichura';
$language["WTTS_WIND_11"] = 'gwałtowna wichura';
$language["WTTS_WIND_12"] = 'huragan';
      
$language["WTTS_MOON_1"] = 'Nów';
$language["WTTS_MOON_2"] = 'Przybywający sierp';
$language["WTTS_MOON_3"] = 'Pierwsza kwadra';
$language["WTTS_MOON_4"] = 'Przybywający garb';
$language["WTTS_MOON_5"] = 'Pełnia';
$language["WTTS_MOON_6"] = 'Ubywający garb';
$language["WTTS_MOON_7"] = 'Ostatnia kwadra';
$language["WTTS_MOON_8"] = 'Ubywający sierp';

//CTS
$language["CTS_HOUR_1"] = 'jedna godzina'; //ein Uhr
$language["CTS_MINUTE_1"] = 'jedna minuta';
$language["CTS_MINUTES"] = 'Minut';
$language["CTS_TEXT"] = 'Teraz jest {hours_12}:{minutes_12} {am_pm}.';

//Traveltiime2TTS
$language['TT2T_PAGE_TITLE'] = 'Ustawienia Traveltime2TTS MS4L';
$language["TT2T_TITLE"] = 'Ustawienia Traveltime2TTS';
$language["TT2T_TITLE_BASIC"] = 'Ustawienia podstawowe';
$language["TT2T_TITLE_DEFAULT"] = 'Ustawienia domyślne';
$language["TT2T_TITLE_TEXT"] = 'Tekst';
$language["TT2T_DEFAULT_APIKEY"] = 'Klucz API - Google';
$language["TT2T_DEFAULT_TRAFFIC"] = 'Weź pod uwagę natężenie ruchu'; 
$language["TT2T_DEFAULT_TRAFFICCALC"] = 'Kalkulacja natężenia ruchu';
$language["TT2T_DEFAULT_OVERWRITE"] = 'Nadpisz teksty domyślnymi w danym języku';
$language["TT2T_TEXT"] = 'Edycja tekstu dla Traveltime';
$language["TT2T_REQ_DEFAULT_APIKEY"] = 'Klucz API musi zostać podany';
$language["TT2T_TEXT"] = 'Na dojazd z {start} do {arrival} co stanowi dystans {distance} kilometrów, w chwili obecnej będziesz potrzebować {duration}.';

//EventCreator
$language["EC_EVNETS"] = 'Events';
$language["EC_TTS"] = 'Text2Speech';
$language["EC_CTS"] = 'Clock2Speech';
$language["EC_RING"] = 'Dzwonek/Budzik/Alarm';
$language["EC_WEATHER"] = 'Weather2Speech';
$language["EC_TRAVEL"] = 'Traveltime2Speech';
$language['EC_PAGE_TITLE'] = 'EventCreator MS4L';
$language["EC_TITLE"] = 'EventCreator';
$language["EC_TITLE_VQ_ADDRESS"] = 'Adres wirtualnego wyjścia';
$language["EC_TITLE_VCQ_COMMANDON"] = 'Komenda wirtualnego wyjścia "Command for ON"';
$language["EC_TITLE_TTS"] = 'Text2Speech';
$language["EC_TITLE_TTS_TEXT"] = 'TTS-Text';
$language["EC_TITLE_CTS_TEXT"] = 'Tekst po komunikacie';
$language["EC_TITLE_W2T_TODO"] = 'Weather Forecast ToDo';
$language["EC_TITLE_TT2T"] = 'Szczegóły dotyczące dojazdu';
$language["EC_TT2T_FROM"] = 'Z lokalizacji';
$language["EC_TT2T_TO"] = 'Do lokalizacji ';
$language["EC_TT2T_TRAFFIC"] = 'Weź pod uwagę natężenie ruchu';
$language["EC_TT2T_TRAFFICCALC"] = 'Kalkulacja natężenia ruchu';
$language["EC_TT2T_DEPARTURE_TIME"] = 'Czas wyjazdu (24h)';
$language["EC_TITLE_RING_FILE"] = 'Plik';
$language["EC_TITLE_ZONEINTERN"] = 'Strefy wewnętrzne';
$language["EC_TITLE_ZONEEXTERN"] = 'Strefy zewnętrzne';
$language["EC_TITLE_OPTIONSBASIC"] = 'Podstawowe';
$language["EC_TITLE_OPTIONSAFTER"] = 'Po Evencie';
$language["EC_TITLE_OPTIONOVERLAY"] = 'Overlay';
$language["EC_TITLE_OPTIONSPECIAL"] = 'Special';
$language["EC_ALL_ZONEINTERN"] = 'Wszystkie wewnętrzne strefy';
$language["EC_ALL_ZONEEXTERN"] = 'Wszystkie zewnętrzne strefy';
$language["EC_VOLUME"] = 'Głośność';
$language["EC_REPEAT"] = 'Powtórz event';
$language["EC_SIGNAL"] = 'Sygnał';
$language["EC_TIMEOUT"] = 'Timeout';
$language["EC_REIMP"] = 'Puls gotowości';
$language["EC_AUTOPLAY"] = 'Autoodtwarzanie';
$language["EC_SYNC"] = 'Synchronizacja';
$language["EC_REPEAT_RESET"] = 'Resetowanie powtarzania';
$language["EC_OVERLAY"] = 'Overlay';
$language["EC_OVERLAYDROP"] = 'Overlay Drop';
$language["EC_PRIO"] = 'Priorytet';
$language["EC_QUEUE"] = 'Kolejka';
$language["EC_SYSTEM_STANDARD"] = 'Używany jest standard systemowy';
$language["EC_NOSIGNAL"] = 'Odtwórz brak sygnału';
$language["EC_GENERATE"] = 'Wygeneruj event';
$language["EC_REQ_TEXT"] = 'Tekst musi być wprowadzony.';
$language["EC_REQ_VOLUME"] = 'Wartość musi być w zakresie 1-100.';
$language["EC_REQ_TIMEOUT"] = 'Wartość musi być większa niż 10.';
$language["EC_REQ_REIMP"] = 'Wartość musi być większa niż 1.';
$language["EC_REQ_ADDRESS"] = 'Adres musi być wprowadzony';
$language["EC_REQ_DEPARTURE_TIME"] = 'Czas musi być w formacie h:m (hours:minutes)';
$language["EC_PH_DEPARTURE_TIME"] = 'h:m (pusty = czas aktualny (teraz))';
$language["EC_STOP"] = 'Zatrzymaj';

//Zone-Tester
?>
